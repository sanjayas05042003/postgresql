
Declare
	counter Integer := 1;
Begin 
	Truncate Table lab_info;
	While counter<=total_seats Loop
		Insert Into lab_info (id)
		Values(counter);
		counter = counter + 1;
	End Loop;
End;