Declare 
	roll_no Integer;
	seat_id Integer;
	available_seats Integer;
	allotted_slot Integer;
	total_seats Integer;
	no_of_allotted_seats Integer;
Begin
	Select max(slot_no) From student_info Into allotted_slot limit 1;
	If allotted_slot Is Null Then
		allotted_slot = 0;
	End If;
	
	Select Count(id) From lab_info Into total_seats;
	Select Count(student_id) From lab_info Into no_of_allotted_seats;
	available_seats = total_seats - no_of_allotted_seats;
				
	If (available_seats = total_seats) Then
		allotted_slot = allotted_slot +1;
	End If;
	
	For roll_no In Select id From student_info Where allotted_seat is null Order By id
	Loop
		--This loop is to select the seats one by one from the lab_info table which are free
		For seat_id In Select id From lab_info Where student_id Is null Order By id
			Loop
				
				Update lab_info 
				Set student_id = roll_no
				Where id =  seat_id;
					
				Update student_info
				Set allotted_seat = seat_id,slot_no = allotted_slot 
				Where id = roll_no;
				
				--available_seats = total_seats - no of allotted_seats 
				Select Count(id) From lab_info Into total_seats;
				Select Count(student_id) From lab_info Into no_of_allotted_seats;
				available_seats = total_seats - no_of_allotted_seats;
				
				If (available_seats = 0) Then
					call end_session();
					allotted_slot = allotted_slot +1;
				End If;
				
				Exit;
			End Loop;
	End Loop;	
End;
