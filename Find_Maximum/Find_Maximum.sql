DECLARE
	columnname record;
	max_value integer := 0;
	current_value integer;
	no_of_rows integer;
	query text;
BEGIN
	query = 'SELECT COUNT(*) FROM ' ||tablename||';';
	EXECUTE query INTO no_of_rows;
	FOR n IN 1 .. no_of_rows
	LOOP
		FOR columnname IN (SELECT column_name FROM information_schema.columns WHERE table_name = tablename)
		LOOP
			query = 'SELECT '||columnname||' FROM '||tablename||' LIMIT 1 OFFSET '||n-1||' ;';
			EXECUTE query INTO current_value;
			IF current_value > max_value THEN
				max_value = current_value;
			END IF;
		END LOOP;
		RAISE NOTICE '%','MAX of row '|| n || ' --> '||max_value;
		max_value = 0;
	END LOOP;
END;